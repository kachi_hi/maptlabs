#include <iostream>

int main() {
    int tab_size = 7;

    int tab[tab_size] = {0,1,2,3,4,5};
    std::cout << " tab= " << tab << std::endl;
    std::cout << " tab[2]= " << tab[2] << std::endl; // tab[n] coresponse for n-th element of the array

    char tab1[tab_size] = "AGH EiT";
    char tab2[tab_size] = {'A','G','H'};
    std::cout << " tab1= " << tab1 << std::endl;
    std::cout << " tab2= " << tab2 << std::endl;
    std::cout << " tab1[2]= " << tab1[2] << std::endl;

    std::cout << "size of tab: " << sizeof(tab) << "B\n";   //size in matter of reserved memmory depends
    std::cout << "size of tab1: " << sizeof(tab1) << "B\n"; //on length and typeof an array
    std::cout << "size of tab2: " << sizeof(tab2) << "B\n"; //length * type size(in bytes)
}
