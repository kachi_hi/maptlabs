#include <iostream>

int main() {
    int tab_size = 7;
    int tab[tab_size] = {0,1,2,3,4,5,6};
    char tab2[tab_size] = "AGH EiT";
    tab[10] = 34;
    for (int i = 0; i < sizeof(tab)/sizeof(tab[0]); i = i + 1 ) {
        std::cout << " i = " << i << " tab[i]= " << tab[i] << " tab2[i]= " << tab2[i] << std::endl;
    }
}