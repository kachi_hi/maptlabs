# lab 7
## ex 1.1:
```cpp
#include <iostream>

int main()
{
    for(int i = 0; 1; i++)
    {   
        std::cout<< i << ", ";
        if(i == 122)
            {
                std::cout<< "\n123 iterations in for loop\n";
                break;    
            }
    }
    int x = 0;
    while(1)
    {
        std::cout<< x << ", ";
        if(x == 122)
            {
                std::cout<< "\n123 iterations in while loop\n";
                break;
            }
        x++;
    }
    int y = 0;
    do
    {
        std::cout<< y << ", ";
        if(y == 122)
            {
                std::cout<< "\n123 iterations in do/while loop\n";
                break;
            }
        y++;
    }
    while(1);

}
```
```
output:
0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 
123 iterations in for loop
0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 
123 iterations in while loop
0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 
123 iterations in do/while loop
```
## ex 1.2:
```cpp
#include <iostream>

int main() {
    int width = 20;
    int height = 10;

    for (int x = 1; x == 1; x = x + 2 ) {
        // for (int y = height; y >= 7; y = y - 3 ) { only 10 and 7 are displayed
        for (int y = height; y >= 1; y = y - 3 ) {
            // if(y == 7)       only 7 will not by displayed
            //     continue;
            std::cout << x << " " << y << std::endl;
        }
    }
}
```
```
output:
1 10
1 7
1 4
1 1
```
## ex 1.3:
```cpp
#include <iostream>

int main(){
	int width = 1920;

	for (int x = 0; x < width; ++x ) {
		// if (x == 2){
		// 	continue;
		// } else if (x == 5){
		// 	break;
		// }
        if( x >= 15 && x <= 30)
        {
            continue;
        }
        else if( x > 50)
        {
            break;
        }
		std::cout << x << std::endl;
	}
}
```
```
output:
0
1
2
3
4
5
6
7
8
9
10
11
12
13
14
31
32
33
34
35
36
37
38
39
40
41
42
43
44
45
46
47
48
49
50
```

## ex 2.1:
```cpp
#include <iostream>

int main() {
    int tab_size = 7;

    int tab[tab_size] = {0,1,2,3,4,5};
    std::cout << " tab= " << tab << std::endl;
    std::cout << " tab[2]= " << tab[2] << std::endl; // tab[n] coresponse for n-th element of the array

    char tab1[tab_size] = "AGH EiT";
    char tab2[tab_size] = {'A','G','H'};
    std::cout << " tab1= " << tab1 << std::endl;
    std::cout << " tab2= " << tab2 << std::endl;
    std::cout << " tab1[2]= " << tab1[2] << std::endl;

    std::cout << "size of tab: " << sizeof(tab) << "B\n";   //size in matter of reserved memmory depends
    std::cout << "size of tab1: " << sizeof(tab1) << "B\n"; //on length and typeof an array
    std::cout << "size of tab2: " << sizeof(tab2) << "B\n"; //length * type size(in bytes)
}
```
```
output:
 tab= 0x7fffec8eae60
 tab[2]= 2
 tab1= AGH EiT
 tab2= AGH
 tab1[2]= H
size of tab: 28B
size of tab1: 7B
size of tab2: 7B
```
## ex 2.2:
```cpp
#include <iostream>

int main() {
    int tab_size = 7;
    int tab[tab_size] = {0,1,2,3,4,5,6};
    char tab2[tab_size] = "AGH EiT";
    tab[10] = 34;
    for (int i = 0; i < sizeof(tab)/sizeof(tab[0]); i = i + 1 ) {
        std::cout << " i = " << i << " tab[i]= " << tab[i] << " tab2[i]= " << tab2[i] << std::endl;
    }
}
```
```
output:
 i = 0 tab[i]= 0 tab2[i]= A
 i = 1 tab[i]= 1 tab2[i]= G
 i = 2 tab[i]= 2 tab2[i]= H
 i = 3 tab[i]= 3 tab2[i]=  
 i = 4 tab[i]= 4 tab2[i]= E
 i = 5 tab[i]= 5 tab2[i]= i
 i = 6 tab[i]= 6 tab2[i]= T
```
## ex 2.3:
```cpp
#include <iostream>

int main() {
    int tab_size = 5;
    int tab[tab_size][tab_size];

    for (int x = 1; x < tab_size; x ++) {
        for (int y = 1; y < tab_size; y ++) {
            tab[x][y]=1;
        }
    }

    std::cout << " tab[x][y] = " << std::endl;

    for (int x = 1; x < tab_size; x ++) {
        std::cout << "\n";
        for (int y = 1; y < tab_size; y ++) {
            std::cout << " " << tab[x][y] ;
        }
    }

    std::cout << "\n\n";
    std::cout << " &tab = " << tab << std::endl;        //address of first element(index 0) of tab
    std::cout << " &tab[1] = " << tab[1] << std::endl;  //address of element with index 1
    std::cout << "size of tab = " << sizeof(tab);       //memory reserved for an array depends on length and type of an array length and size of type in this example array 5x5(25 elements) * 4B(int)
}
```
```
output:
 tab[x][y] = 

 1 1 1 1
 1 1 1 1
 1 1 1 1
 1 1 1 1

 &tab = 0x7ffc53cc7a90
 &tab[1] = 0x7ffc53cc7aa4
size of tab = 100
```