#include <iostream>

int main()
{
    for(int i = 0; 1; i++)
    {   
        std::cout<< i << ", ";
        if(i == 122)
            {
                std::cout<< "\n123 iterations in for loop\n";
                break;    
            }
    }
    int x = 0;
    while(1)
    {
        std::cout<< x << ", ";
        if(x == 122)
            {
                std::cout<< "\n123 iterations in while loop\n";
                break;
            }
        x++;
    }
    int y = 0;
    do
    {
        std::cout<< y << ", ";
        if(y == 122)
            {
                std::cout<< "\n123 iterations in do/while loop\n";
                break;
            }
        y++;
    }
    while(1);

}